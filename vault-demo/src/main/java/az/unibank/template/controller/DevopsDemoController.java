package az.unibank.template.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public  class DevopsDemoController {

    @Value("${api.name}")
    private String user;

    @Value("${api.password}")
    private String password;

    @GetMapping("/vault")
    public String  vaultDemo(){

     return "Your username is " + user + " and password is " + password;
    }
}
