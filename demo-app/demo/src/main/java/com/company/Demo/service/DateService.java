package com.company.Demo.service;

import com.company.Demo.config.ApplicationProperties;
import com.company.Demo.model.DateDto;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DateService {

    private final ApplicationProperties properties;
    public DateDto getDate() {
        return DateDto.builder()
                .env(properties.getEnv())
                .date(LocalDateTime.now())
                .build();
    }
}
